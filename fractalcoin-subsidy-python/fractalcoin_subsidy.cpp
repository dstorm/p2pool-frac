#include <Python.h>

static const int64_t COIN = 100000000;

int64_t GetBlockBaseValue(int nHeight)
{
    //1M coins total. 5000 premine, 40 per block after, etc etc
    int64_t reward = 10 * COIN;
    int adjust=30+1; //30 blocks of zero rewards, to adjust difficulty and ensure fair launch (plus 1 for premine)
    int firstreward=adjust+1*60*24; //first day of mining 
    int secondreward=firstreward+(13*60*24); //next 13 days of mining
    int thirdreward=secondreward+(13*60*24); //next 13 days of mining
    int fourthreward=thirdreward+(1*60*24); //last day of "primary mining"
    int finalreward=fourthreward+((60*24*221)-40); //final reward phase (after this, nothing)
    if(nHeight==1)
    {
        return 500*reward; //premine of 5000 coins, 0.5% of cap
    }
    else if(nHeight < adjust)
    {
        return 0;
    }
    else if(nHeight < firstreward)
    {
        return reward*4; //bonus phase of 40 coins 
    }
    else if(nHeight<secondreward)
    {
        return reward*2; //normal first phase of 20 coins
    }
    else if(nHeight<thirdreward)
    {
        return reward*1; //normal last phase of 10 coin
    }
    else if(nHeight<fourthreward)
    {
        return reward*4; //end primary mining with a bang round of 40 coins
    }
    else if(nHeight<finalreward)
    {
        return reward/10; //final phase is 1 coin reward
    }
    else
    {
        return 0; //no coins
    }
}

static PyObject *fractalcoin_subsidy_getblockbasevalue(PyObject *self, PyObject *args)
{
    int input_height;
    if (!PyArg_ParseTuple(args, "i", &input_height))
        return NULL;
    long long output = GetBlockBaseValue(input_height);
    return Py_BuildValue("L", output);
}

static PyMethodDef fractalcoin_subsidy_methods[] = {
    { "GetBlockBaseValue", fractalcoin_subsidy_getblockbasevalue, METH_VARARGS, "Returns the block value" },
    { NULL, NULL, 0, NULL }
};

PyMODINIT_FUNC initfractalcoin_subsidy(void) {
    (void) Py_InitModule("fractalcoin_subsidy", fractalcoin_subsidy_methods);
}
