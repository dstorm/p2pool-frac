from distutils.core import setup, Extension

fractalcoin_module = Extension('fractalcoin_subsidy', sources = ['fractalcoin_subsidy.cpp'])

setup (name = 'fractalcoin_subsidy',
       version = '1.0',
       description = 'Subsidy function for FractalCoin',
       ext_modules = [fractalcoin_module])
